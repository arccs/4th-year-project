# ARCCS - Accessible RC Car System

[TOC]

## Project Overview

Conventional RC Car Controllers are difficult for people with limited fine motor skills to use. ARCCS (Accessible RC Car System) is a custom designed embedded system that utilizes a wheelchair joystick for RC Car control.

The following outlines algorithms and processes in the ARCCS C Program at a high level — for full details the code must be read (See STM Source Code). Target device is the STM32F103RB processor on the STM32VLDiscovery Board. This ReadMe only covers software.


The repo itself contains all relevant electrical schematics, PCB design files, photos, and other information. 

![ReadMeHeader.jpg](https://bitbucket.org/repo/z5E8jo/images/2401759187-ReadMeHeader.jpg)


### Scope/Functions

- To built a device that controls RC Cars via a wheelchair joystick instead of the current trigger and wheel system.
- The device must have a speed control dial, so the model’s speed can be adjusted to slow or progressively faster.
- The device must have a “progressive acceleration” function (with a switch that could be activated or deactivated)
- The device must be capable of controlling different types of propulsion systems (with a switch):
  - 4 wheels with steering
  - 2 tracks (Tanks, Constuction Equipment, etc.)
- The device must be capable of detecting and avoiding objects

### Main Algorithm (High level)

1. Fetch the ADC values from the Direct Memory Access peripheral (Joystick X input, Y input, Speed Control, etc.)
2. Check the special function switches, if enabled run seperate subrountines 
   - Figure 8 Mode
   - Self-Driving Mode
   - Object Detection ON/OFF
3. Check the mode dipswitch
   - 4 Wheel Mode 
   - 2 Track Mode
4. Perform Joystick Control Routine
   - Process current ADC input and convert it to a `uint_16t`value from `0-255`
   - Limit output value based on speed potentiometer input
   - Check Progressive Acceleration switch and run subroutine if enabled 
   - Check for objects and avert if necessary
   - Output user value to the 10K digital potentiometer (used to control the transmitter throttle and steering inputs)



Two separate modules are used in this project. 

1. The main case, containing all IO input, the STM board, and the receiver xBee (for object avoidance)
2. The car case, containing IR sensors and the transmitter xBee (for object avoidance)



## Hardware/Pin Overview

### GPIO Pins Table

| IO Description                     | Type                 | Hardware        | Code File Declared      | Code Function      | STM Process | GPIO Pin |
| :--------------------------------- | -------------------- | --------------- | ----------------------- | ------------------ | ----------- | -------- |
| Joystick X input                   | Analog Input         | Joystick Sensor | joystick_periph_setup.c | joystick_ADC_init  | ADC         | PA0      |
| Joystick Y input                   | Analog Input         | Joystick Sensor | joystick_periph_setup.c | joystick_ADC_init  | ADC         | PA1      |
| Speed Control                      | Analog Input         | Sliding Pot     | joystick_periph_setup.c | multiple           | ADC         | PA2      |
| Object Detection Trim              | Analog Input         | Pot             | joystick_periph_setup.c | multiple           | ADC         | PA3      |
| Joystick VREF                      | Analog Input         | Joystick Sensor | joystick_periph_setup.c | joystick_ADC_init  | ADC         | PA       |
| Operation Mode (4 Wheels/2 Tracks) | Digital Input        | Color Switch    | IO_processing.c         | get_Mode           | GPIO ODR    | PC8      |
| Progressive Acceleration           | Digital Input        | Color Switch    | joystick_processing.c   | progressive_accell | GPIO ODR    | PC9      |
| Object Detection ON/OFF            | Digital Input        | Color Switch    | joystick_processing.c   | multiple           | GPIO ODR    | PC10     |
| Assistive Steering                 | Digital Input        | Color Switch    | joystick_processing.c   | assist_steering    | GPIO ODR    | PC11     |
| Figure 8                           | Digital Input        | Color Switch    | automation_processing.c | multiple           | GPIO ODR    | PC12     |
| EXTRA                              | Digital Input        | Color Switch    |                         |                    | GPIO ODR    | PB7      |
| USART LED                          | Digital Output       | Color Switch    | object_avoidance.c      | multiple           | GPIO ODR    | PB8      |
| SPI MISO                           | Digital Output (SPI) | Digital Pot     | SPI.c                   | digitalPotWrite    | SPI         | PB14     |
| SPI SCK                            | Digital Output (SPI) | Digital Pot     | SPI.c                   | digitalPotWrite    | SPI         | PB13     |
| SPI SS                             | Digital Output (SPI) | Digital Pot     | SPI.c                   | digitalPotWrite    | SPI         | PB12     |
| USART1 TX                          | Digital Output       | xBee            | UART.c                  | multiple           | USART1      | PA9      |
| USART1 RX                          | Digital Input        | xBee            | UART.c                  | multiple           | USART1      | PA10     |
| USART1 CTS                         | Digital Output       | xBee            | UART.c                  | multiple           | USART1      | x        |
| USART1 RTS                         | Digital Input        | xBee            | UART.c                  | multiple           | USART1      | x        |
| RSSI Input                         | Digital Input        | xBee            | NOT USED                | NOT USED           | GPIO        | PB9      |
| USART3 TX                          | Digital Output       | Jessies Project | TBD                     | TBD                | USART2      | PB10     |
| USART3 RX                          | Digital Input        | Jessies Project | TBD                     | TBD                | USART2      | PB11     |
| LCD RS                             | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PB0      |
| LCD RW                             | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PB2      |
| LCD E                              | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PB1      |
| LCD DB0                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC0      |
| LCD DB1                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC1      |
| LCD DB2                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC2      |
| LCD DB3                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC3      |
| LCD DB4                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC4      |
| LCD DB5                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC5      |
| LCD DB6                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC6      |
| LCD DB7                            | Digital Output       | LCD             | LCD_lib.c               | LCD_init           | GPIO        | PC7      |



### STM Peripherals Used

- SPI2 - Used to communicate with the AD5206 Digital Potentiometer 
- USART1 - Used to communicate with the Object Detection xBEE
- ADC1 - Used to read in analog inputs (Channels 1 to 5) 
- Direct Memory Access (DMA) - Skips the CPU when reading in ADC inputs, stores values directly in flash memory

## File Overview

### main.c/h

- STM32 peripheral initialization functions
- Direct Memory Access (DMA) initialization 
  - Required for Analog-to-Digital Conversion
- Main control loop 
- Helper functions (Fetch global ADC Variables for example)

### IO_processing.c/h

- Reading in discrete inputs (function switches)

### joystick_processing.c/h

- Main control code for joystick 4-wheel and 2-track mode
- Speed Control Routines and Look-up Tables 
- Progressive Acceleration Routine
- Tuning/Calibration Function

### automation_processing.c/h

- Functions relating to the automation of the cars movements
- Figure 8 mode

### UART.c/h

- xBee to STM peripheral initialization and read/write routines

### SPI.c/h

- Initialization of SPI peripheral for communication with digital pot
- Read/write functions for the digital potentiometer

*Note: All other files are self-explanatory and well documented in the code.*

### object_avoidance.c/h

- Reading USART data from the receiving xBee
- Object Avoidance control algorithms
- Self-Driving mode algorithm

## Joystick ADC Input

### Direct Memory Access (DMA)

In order to increase program efficiency in reading and processing the required 5 Analog Inputs, the Direct Memory Access feture of the ARM Cortex-M3 processor is utilized.



> Direct memory access (DMA) is a feature of computer systems that allows certain hardware subsystems to access main system memory (RAM) independently of the central processing unit (CPU).
>
> Without DMA, when the CPU is using programmed input/output, it is typically fully occupied for the entire duration of the read or write operation, and is thus unavailable to perform other work. With DMA, the CPU first initiates the transfer, then it does other operations while the transfer is in progress, and it finally receives an interrupt from the DMA controller when the operation is done. This feature is useful at any time that the CPU cannot keep up with the rate of data transfer, or when the CPU needs to perform useful work while waiting for a relatively slow I/O data transfer. 

Source: https://en.wikipedia.org/wiki/Direct_memory_access



By using the DMA to fetch the ADC values the program can focus solely on execting the main control loop — focusing on high priority functions such as: 

- Processing progressive acceleration 
- Processing speed control 
- Sending values to the Digital Potentiometers via SPI 
- Reading the USART port for the xBee object detection

Functionally, the DMA reads all 5 ADC inputs into a buffer array `ADC_values[5]` stored in SRAM at address `0x4001244C`.  Each address of the `ADC_values[5]` array corresponds to the ADC values of each GPIO Pin. 



Ex: `ADC_values[0] = ADC value on pin PA0`



### ADC Input Hardware

The program relies on inputs from a JC200-B-R-2-K1-Y wheelchair joystick. Input to the joystick is 12VDC and GND. Outputs from the joystick are as follows: 

- Yellow Wire - X Position
- Blue Wire - Y Position 
- Green Wire - Centre Tap (1/2 VDC)

*Note that the X and Y inputs on the joystick were reversed for this project. The datasheet for the joystick will spec yellow as Y and blue as X. We chose to reverse this in order to use the plastic divit on the joystick as a reference for forward. The voltage references for each are the same — thus this is a non issue.*

X and Y outputs from the joystick are voltages from 5-7VDC, with 7V representing the max reverse positions and 5V representing the max forward position. The STM board is rated for 0-3V analog inputs. In order to safely read the joystick values into the board they are fed through a voltage divider and a voltage follower op amp. This safely reduces the voltages to a range from ~2V-3V. 

Inputs from the 16-bit ADC are processed and converted to represent a `uint16_t` value from 0-255. With values from 127-255 representing the reverse range of the RC Car (255 is max reverse speed — and 0-127 representing the forward range of the RC Car (0 is max forward speed). 



### Input Calibration

Typical output values from the joystick as follows: 

*Higher ADC values on the Y-Axis represent reverse throttle.*

*Higher ADC values on the X-Axis represent right turning.*



#### Y-Axis ADC Input Table

| Voltage | ADC  | Updated ADC | Pot Value | Resistance |
| ------- | ---- | ----------- | --------- | ---------- |
| 2.8     | 3950 | 1252        | 255.00    | 10000.00   |
| 2.75    | 3866 | 1168        | 237.89    | 9329.07    |
| 2.7     | 3800 | 1102        | 224.45    | 8801.92    |
| 2.65    | 2730 | 32          | 6.52      | 255.59     |
| 2.6     | 3660 | 962         | 195.93    | 7683.71    |
| 2.55    | 3590 | 892         | 181.68    | 7124.60    |
| 2.5     | 3515 | 817         | 166.40    | 6525.56    |
| 2.45    | 3445 | 747         | 152.14    | 5966.45    |
| 2.4     | 3380 | 682         | 138.91    | 5447.28    |
| 2.37    | 3340 | 642         | 130.76    | 5127.80    |
| 2.35    | 3300 | 602         | 122.61    | 4808.31    |
| 2.3     | 3233 | 535         | 108.97    | 4273.16    |
| 2.25    | 3160 | 462         | 94.10     | 3690.10    |
| 2.2     | 3100 | 402         | 81.88     | 3210.86    |
| 2.15    | 3030 | 332         | 67.62     | 2651.76    |
| 2.1     | 2955 | 257         | 52.34     | 2052.72    |
| 2.05    | 2880 | 182         | 37.07     | 1453.67    |
| 2       | 2815 | 117         | 23.83     | 934.50     |
| 1.95    | 2740 | 42          | 8.55      | 335.46     |
| 1.9     | 2698 | 0           | 0.00      | 0.00       |



#### X-Axis ADC Input Table

| Voltage | ADC  | Updated ADC | Pot Value | Resistance |
| ------- | ---- | ----------- | --------- | ---------- |
| 2.85    | 3915 | 1215        | 255.00    | 10000.00   |
| 2.8     | 3845 | 1145        | 240.31    | 9423.87    |
| 2.75    | 3780 | 1080        | 226.67    | 8888.89    |
| 2.7     | 3710 | 1010        | 211.98    | 8312.76    |
| 2.65    | 3637 | 937         | 196.65    | 7711.93    |
| 2.6     | 3570 | 870         | 182.59    | 7160.49    |
| 2.55    | 3510 | 810         | 170.00    | 6666.67    |
| 2.5     | 3420 | 720         | 151.11    | 5925.93    |
| 2.45    | 3365 | 665         | 139.57    | 5473.25    |
| 2.4     | 3345 | 645         | 135.37    | 5308.64    |
| 2.35    | 3230 | 530         | 111.23    | 4362.14    |
| 2.3     | 3165 | 465         | 97.59     | 3827.16    |
| 2.25    | 3095 | 395         | 82.90     | 3251.03    |
| 2.2     | 3025 | 325         | 68.21     | 2674.90    |
| 2.15    | 2950 | 250         | 52.47     | 2057.61    |
| 2.1     | 2880 | 180         | 37.78     | 1481.48    |
| 2.05    | 2820 | 120         | 25.19     | 987.65     |
| 2       | 2750 | 50          | 10.49     | 411.52     |
| 1.95    | 2700 | 0           | 0.00      | 0.00       |



In order for the joystick to operate properly it is necessary to determine the centre, max,  and min points of the X and Y positions (whenever the 12V input voltage is deviated from). Since the joystick outputs a voltage to the STM ADC from ~2V to ~3V the range of ADC inputs is not linear from 0-4096 (more like 2500-4000). 

In order for the joystick processing code to work, calibration is necessary to scale the ADC values to 0-Z (z being the value at the scaled 3v input). With a scale from 0-Z it is easy to set a digital pot output from 0-255. Calibration is necessary for running off any voltage aside from the specced 12v. 



#### Steps to Calibrate

1. Read the voltage on the Y output of the LM358N Op Amp
2. Print the Y ADC values to the an LCD or view in the Keil Debugger (main while loop) 
3. Record the min, max and centre point voltages and ADC values
4. Let `scale_factor = the minimum adc value `
5. `division_factor = max ADC value - scale_factor`
6. Enter the values into their respective variables in the following functions (joystick_processing.c)

```c
uint16_t get_scale_factor(uint16_t centre_ADC_val){
	return 2715; // Enter value here
}
```

```c
uint16_t get_division_factor(uint16_t centre_ADC_val){
	return 1295; // Enter value here
}
```



## RC Control via Digital Potentiometers (SPI)

### Determining Digital Pot Values

The following describes how the joystick ADC inputs are converted to an appropriate digital pot value.



We use the previously determine `scale_factor` to scale the 16-bit ADC input values (0-4095) to a new scale from 0-X. First we define a `float` to hold the new scaled ADC values. Then the actual ADC input is scaled down to the new range. 

```c
float scaled_ADC_x;
float scaled_ADC_y;

scaled_ADC_x  = x_ADC_val - scale_factor; 
scaled_ADC_y  = y_ADC_val - scale_factor; 
```

Next, we determine the percentage of the ADC input with respect to the max scaled ADC value. This corresponds physically to the joysticks actual position — say 60% forward. 

```c
double percent_x;
double percent_y;

percent_x =  scaled_ADC_x/division_factor;
percent_y =  scaled_ADC_y/division_factor;
```

Finally, the percentage value is used to determine the position of the digital potentiometer (0-255). Note that this corresponds to a percentage of the 10K maximum output. 

```c
uint16_t x_pot_val; 
uint16_t y_pot_val; 

x_pot_val = (uint16_t)(percent_x * 255); 
y_pot_val = (uint16_t)(percent_y * 255); 	
```

Finally, we use SPI to send two values to the AD5206N digital potentiometer — the Channel and the Value. 

- Channel = Pot output from 0-5
- Value = Pot value from 0-255

```c
// Write values to the digital pots
digitalPotWrite(CHANNEL_0, x_pot_val);
digitalPotWrite(CHANNEL_1, y_pot_val);
```

Output at this point corresponds to 1:1 control of the RC car — with joystick position directly relating to throttle and speed. 



*For further reference on SPI and digital potentiometers see the SPI.c source code file. Note the the above only applies to 4-wheel RC Car control — 2 track vehicles are covered in a later section*



## Speed Control

As defined in the scope of the project the RC max car's speed must also be controlled. To do this a 10K sliding potentiometer is read into ADC Channel 3 (Pin PA2).  The input voltage to the pot is 3.3V which corresponds to an ADC input range from 0-4095. This value is read into the ADC DMA and converted to a digital potentiometer value (from 0-255) — so that it can be easily compared to maximum digital-potentiometer output values. 



Recall that the integer digital potentiometer values corresponds to the following thrust movements on the RC Car:

| Digital Potentiomenter Value | Car Movement      |
| ---------------------------- | ----------------- |
| 255 - 10K                    | Max Reverse Speed |
| 131 - 5.14K                  | No Movement       |
| 0 - 0K                       | Max Forward Speed |



### Algorithm

*Note: Speed Control Implementation is split over many functions, the following is only a high level overview of the algorithm — not one signal function.*



#### Pseudocode

```c
fetch the value of the speed control pot - convert to a uint16_t from 0-255
 	set pot_val to this uint16_t 
set max_reverse_speed to the reverse_speed_LookupTable[pot_val/2]
set max_forward_speed to the forward_speed_LookupTable[pot_val/2]

// In main control loop 
if joystick_y_position > max_reverse_speed
   joystick_y_position = max_reverse_speed 
else if joystick_y_position < max_forward_speed
   joystick_y_position = max_forward_speed 

output joystick_y_position to digital potentiometer throttle channel 
```



#### Lookup Table

In order to determine the max speed the car should travel (based on the analog potentiometer input) it is necessary to determine both the maximum forward and reverse speed. 

For example: If the speed control pot is set to 50% of max speed (3.3v/2 = 1.65v) the program must recognize that the Max Reverse Speed must be limited to 50% of 255 and the Min Forward Speed must be limited to 50% of 131. 

To do this a lookup-table (LUT) approach is used. The lookup table determines both the value of speed to output for max reverse and forward senarios. By using a lookup-table the processing time is improved as no floating-point calculations need to be performed. 



The lookup tables are as follows — note the const keyword is used to ensure the data is stored in FLASH memory and thus save RAM space for executable code: 

```c
static const uint16_t reverse_speed_LUT[129] =
{	
  128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,	
  141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153,	
  154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166,	
  167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,	
  180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192,	
  193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205,	
  206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218,	
  219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231,	
  232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244,	
  245, 246, 247, 248, 249	
};

static const uint16_t forward_speed_LUT[130] =
{	
  131, 131, 131, 131, 124, 123, 122, 121, 120, 119, 118, 117, 116,	
  115, 114, 113, 112, 111, 110, 109, 108, 107, 106, 105, 104, 103,	
  102, 101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87,	
  86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70,	
  69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 
  52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36,	
  35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19,	
  18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 
};
```



Using the value determined from the `get_speed_pot` function, the max forward and reverse speeds are fetched from the LUTs. Note that the `get_speed_pot` value is divided by 2 — because the effective resolution of the range form max forward/reverse to min forward/reverse (0-131 % 131-255) is 255/2. 

```c
uint16_t get_max_reverse_speed(uint16_t speed_ADC_Val){
	uint16_t max_reverse_speed;		// Max Speed value 
	max_reverse_speed = (get_speed_pot(speed_ADC_Val))/2; 
	return reverse_speed_LUT[max_reverse_speed]; 	// Fetch speed from LUT
 }

 uint16_t get_max_forward_speed(uint16_t speed_ADC_Val){
 	uint16_t max_forward_speed;		// Max Speed value 
 	max_forward_speed = (get_speed_pot(speed_ADC_Val))/2; 
 	return forward_speed_LUT[max_forward_speed]; 	// Fetch speed from LUT
 }
```



#### Example Scenario

- Speed Control potentiometer is set to 30% of the maximum value

  `30% of 3.3v = 1v`

- Speed Control voltage is read into the ADC (0-4095) and converted to a value from 0-255

  `30% of 255 = 77`

- The minimum and maximum reverse and forward speeds are fetch from the lookup table 

  `77/2 = 39 `

  `reverse_speed_LUT[39] = 168`

  `30% of the reverse range values (131-255) = (131)*1.3 = 170`

  `forwad_speed_LUT[39] = 92 `

  `30% of the max forward range values = 131*0.7 = 91.7`1

Thus. the lookup table returns a value within reason compared to the manually calculated value, and saves significant processing time by avoiding floating point operations. 



*Note: Further optimizations are made by reducing the Speed Control ADC sample time to every 55 clock cycles — for comparision the highest priority ADC input (Joystick values) are sampled every 28.5 Cycles. This design decision is justified because the Speed Control is not as high priority as Joystick input processing.*



## Progressive Acceleration

Progressive Accelerationn slowly ramps up or down the speed of the RC Car upon user input to the joystick. Normally, full application of the joystick in the Y (forward)  corresponds to the maximum throttle speed of the car. With progressive acceleration enabled the car's speed starts at 0 and slowly ramps up to the user's position on the joystick throttle axis. When enabled, throttle speed is no longer proportional but instead based solely on the speed control potentiometer value. For example, when the joystick is moved anywhere in the forward Y-direction, the program will slowly ramp the speed up until the max forward speed limit is reached. 

    

If the progressive acceleration switch is enabled the main control loop breaks and runs the `progressive_accel` function. If not the 1:1 control described above occurs and no control processing is done on the car's throttle. 

​    

### Algorithm

#### Input

```c
uint16_t max_reverse_speed	 // Preserve Reverse Speed-Control 
uint16_t max_forward_speed 	 // Preserve Forward Speed-Control 
float scaled_ADC_y			// Used to determine desired direction of travel
```

#### Output

`void ` — however values are written in-function to the digital potentiometers 



#### Pseudocode

```c
function progressive_accel begin  

check direction [FW/REV] based on y-axis joystick position
  static bool moving -- set based on direction 

if moving is true 
  if direction is FW
    for digital-pot-value is not moving to max forward speed  
      if direction is REV or moving is false
        return 

      delay 0.2 second 	// Used to simulate gradual acceleration 

      sample and fetch current x-axis joystick position
        output x-axis to the steering digital potentiometer 
      output digital-pot-value (loop condition) to the y-axis digital pot 

      moving is false 
   else if direction is REV
    for digital-pot-value is not moving to max reverse speed  
      if direction is REV or moving is false
        return 

      delay 0.2 second // Used to simulate gradual acceleration 

      sample and fetch current x-axis joystick position
        output x-axis to the steering digital potentiometer 
      output digital-pot-value (loop condition) to the y-axis digital pot 

      moving is false 
```



#### Notes

- Steering (x-axis joystick value) is sampled each iteration of the for-loop to ensure that there is no noticeable delay in steering-control
- The 0.2 second delay was chosen by 'feel' and user-testing trails
  - larger delay value = slower acceleration time
    ​

## Object Avoidance

Object avoidance uses xBees and IR sensors to gather object positioning data from an on car module (separate from the main case). 

The transmitter xBee is configured to read in 3 Analog inputs and send them over USART Serial to the receiver xBee (connected to the STM). The 3 IR sensors correspond to a LEFT, MIDDLE, and RIGHT object. 



### USART Algorithm

Data from the xBee is read into the STM on Pin PA10 (RX). Data is queried on each iteration of the main control loop. Usart data is read into a 36 byte buffer array (uint16_t) in order to create a data frame. The 36 byte data frame is then processed and the xBee Analog Input bytes are taken out and stored in separate variables (for the left, middle, and right sensors). For more information on the xBee configurations and processing refer to the 1mW Wire Antenna xBee manual.  

Using the data gathered from the xBees, the object positions are compared to the gain threshold pot (Analog Input PA3). This potentiometer determines the threshold for object detection. If the threshold is set high (Max 50cm) then objects will be detected as far as that distance. If the threshold is set low (Min 20cm) then object will be detected as far as that distance. 

The function `int object_detect()`  in `object_avoidance.c` processes the USART input buffer and returns an integer value corresponding to an objects position. 

| Integer Returned | Object Position                     |
| ---------------- | ----------------------------------- |
| 0                | No object                           |
| 1                | Object on Left Corner               |
| 2                | Object on Right Corner              |
| 3                | Object on Left                      |
| 4                | Object on Right                     |
| 5                | Object in Middle                    |
| 10               | USART Timeout (Communications loss) |

If the USART times out or the on car module (housing the transmitter xBee) is not turned on, the program will default the car to a stopped position and blink an LED to indicate communications loss. 



#### Object Avoidance Pseudocode

Using the integer values determined from the  `int object_detect()`  function the cars position can be changed if an object is detected. 

The basic algorithm is as follows: 

```c
function avoid_object
 	if object_position = 0 
      // no object
      do nothing 
    if object_position = 1
      // Object on left corner
      go slight right 
    if object_position = 2
      // Object on right corner
      go slight left
    if object_position = 3
      // Object on left
      go full right
    if object_position = 4
      // Object on right
      go full left
    if object_position = 5
      stop the car
      only allow reverse movements
end func
```

For further details see the code. 



#### Self-Driving​ Mode

Self-driving mode uses the values returned from  `int object_detect()`  to navigate around obstacles autonomously. 

The basic algorithm is as follows: 

```c
function avoid_object_self_driving
 	if object_position = 0 
      // no object
      go forward and straight
    if object_position = 1
      // Object on left corner
      go slight right and forward
      if (object_position = 1 for 3 seconds)
        go slight right and reverse 
    if object_position = 2
      // Object on right corner
      go slight left and forward
      if (object_position = 2 for 3 seconds)
        go slight left and reverse 
    if object_position = 3
      // Object on left
      go right and forward
      if (object_position = 3 for 3 seconds)
        go right and reverse 
    if object_position = 4
      // Object on right
      go left and forward
      if (object_position = 4 for 3 seconds)
        go left and reverse 
    if object_position = 5
      stop the car
      if (object_position =54 for 3 seconds)
        go left and reverse

end func
```

For further details see the code. 



## Special Functions

### Figure 8 Mode

Figure 8 mode makes the car go in a right-ward figure 8 motion with the size of the loops dependent on the y-joystick position. The further the joystick is pressed forward the larger the loop. To reduce complexity speed is limited in this function and thus the speed pot has no effect. 



#### Pseuedocode

```c
function figure_eight begin
  if (y-joystick position is in resting position)
    return 	// do nothing 

  if (y-joystick position is < 130 and > 110)
    delay_time = 3000
  else if (y-joystick position is <= 110 and > 90)
    delay_time = 4000
  else if (y-joystick position is <= 90 and > 60)
    delay_time = 5000
  . . .  
  else if (y-joystick position is <= 20 and > 0)
    delay_time = 10000 

  go_right for delay_time 
  go_left for delay_time
  repeat
```



## Final Notes

Please see the program files for all other documentation and details. A full understanding of the program and system requires an in-depth study of the program code.