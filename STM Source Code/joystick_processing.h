/******************************************************************************
 * Name:    joystick_processing.c
 * Description: Joystick Processing Functions 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains the main control code for joystick operation
 * 					main function of concerin is standard_RC_control
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include <stdbool.h>

/* Private macros ------------------------------------------------------------*/
// Digital Pot Channels 
#define STEERING			0
#define THROTTLE			1

/* Function Prototypes ------------------------------------------------------------------*/
// 4-Wheel RC
void standard_RC_control(uint16_t, uint16_t, uint16_t, uint16_t, int, int, int, int);
void progressive_accel(uint16_t, uint16_t, float, bool); 
uint16_t assist_steering(uint16_t); 
int get_direction(uint16_t); 

// 2-Track RC
void two_track_RC_control(uint16_t, uint16_t, uint16_t, uint16_t, int, int, int, int);
void perform_two_track_operations(int, uint16_t, uint16_t);

// Other
uint16_t get_speed_pot(uint16_t);
uint16_t get_max_forward_speed(uint16_t);
uint16_t get_max_reverse_speed(uint16_t);
uint16_t get_scale_factor(int);
uint16_t get_division_factor(int);
uint16_t adjust_speed(uint16_t, uint16_t, uint16_t);
void reverse_assist(int reverse_adjust_flag, uint16_t y_pot_val, int boost_reverse);
void zero_pots(void);
uint16_t get_speed_self_driving(uint16_t);

