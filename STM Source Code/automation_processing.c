/******************************************************************************
 * Name:    automation_processing.h
 * Description: Functions relating to automating the cars movements 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Figure 8 control, and other semi-automonous modes
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "joystick_processing.h"
#include "automation_processing.h"
#include "SPI.h"
#include "LCD_lib.h"
#include "main.h"
#include "joystick_periph_setup.h"
#include "object_avoidance.h"
#include "IO_processing.h"
#include <stdlib.h>     
#include <stdbool.h>
#include "math.h"

 
//------------ Automation Backend Functions ------------//
/*
* Name:					boost_reverse()
* Paramaters: 	void
* Description:  Tricks the RC car into going into reverse mode instead of breaking mode
*/
void boost_reverse(void){
	int boost_reverse = 131; 
	
	// Force forward output to the car to go from 'brake' to 'reverse' 
	for (boost_reverse = 115; boost_reverse < 133; boost_reverse++){
		commandToLCD(LCD_LN1);
		intToLCD(boost_reverse); 	
		digitalPotWrite(THROTTLE, boost_reverse);
	}
}


/*
* Name:					go_forward()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Drives the car forward depending on a delay time (which comes from the Y_Axis Input) 
*/
void go_forward(uint16_t y_ADC_val){
	uint16_t delay_time = y_ADC_val * 100;	
	int i = 0;
	
	if (y_ADC_val >= 3200){		// Going in reverse 
		digitalPotWrite(STEERING, 125);
		digitalPotWrite(THROTTLE, STOPPED);		
	}else{

		for (i=0; i < delay_time; ++i)
		{
			digitalPotWrite(STEERING, 125);
			digitalPotWrite(THROTTLE, MIN_FW_SPEED);
		}
}
}


/*
* Name:					go_backward()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Drives the car backward depending on a delay time (which comes from the Y_Axis Input) 
*/
void go_backward(uint16_t y_ADC_val){
	uint16_t delay_time = y_ADC_val * 100;
	int i = 0; 

	if (y_ADC_val >= 3200){		// Going in reverse 
		digitalPotWrite(STEERING, 125);
		digitalPotWrite(THROTTLE, STOPPED);		
	}else{
		for (i=0; i < delay_time; ++i)
		{
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
	}
}


/*
* Name:					go_left()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Drives the car Left depending on a delay time (which comes from the Y_Axis Input) 
*/
void go_left(uint16_t y_ADC_val, uint16_t speed_ADC_val, int scale_factor, int division_factor, uint16_t steer_dir){
	float scaled_ADC_y;
	double percent_y;		
	bool object_was_detected; 
	uint16_t y_pot_val;
	uint16_t max_reverse_speed = get_max_reverse_speed(speed_ADC_val);	
	uint16_t max_forward_speed = get_max_forward_speed(speed_ADC_val);	
	
	scaled_ADC_y  = y_ADC_val - scale_factor; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor;
	y_pot_val = (uint16_t)(percent_y * 255); 

	// Adjust speed based on limits 
	y_pot_val = adjust_speed(get_y_ADC(), max_reverse_speed, max_forward_speed); 
	
	if (y_ADC_val >= 3600){
		digitalPotWrite(STEERING, 125);
		digitalPotWrite(THROTTLE, STOPPED);		
	}else{
		if (object_was_detected == false){
			digitalPotWrite(THROTTLE, y_pot_val);		
			digitalPotWrite(STEERING, steer_dir);		
		}
	}
}


/*
* Name:					go_right()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Drives the car right depending on a delay time (which comes from the Y_Axis Input) 
*/
void go_right(uint16_t y_ADC_val, uint16_t speed_ADC_val, int scale_factor, int division_factor, uint16_t steer_dir){
	float scaled_ADC_y;
	double percent_y;		
	bool object_was_detected; 
	uint16_t y_pot_val;
	uint16_t max_reverse_speed = get_max_reverse_speed(speed_ADC_val);	
	uint16_t max_forward_speed = get_max_forward_speed(speed_ADC_val);	
	
	scaled_ADC_y  = y_ADC_val - scale_factor; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor;
	y_pot_val = (uint16_t)(percent_y * 255); 

	// Adjust speed based on limits 
	y_pot_val = adjust_speed(get_y_ADC(), max_reverse_speed, max_forward_speed); 

	if (y_ADC_val >= 3600){			// User is going backwards
		digitalPotWrite(STEERING, 125);
		digitalPotWrite(THROTTLE, STOPPED);		
	}else{
		if (object_was_detected == false){
			digitalPotWrite(THROTTLE, y_pot_val);		
			digitalPotWrite(STEERING, steer_dir);		
		}
	}
}


//------------ Automation Routines ------------//
/*
* Name:					figure_eight()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Automatically perform figure eights - size dependent on y-axis output
*/
void figure_eight(uint16_t y_ADC_val, uint16_t speed_ADC_val, int scale_factor, int division_factor){
	uint16_t delay_time;			// Time between completion of circles 
	uint16_t steer_dir_left; 
	uint16_t steer_dir_right;
	uint16_t y_pot_val ; 
	float scaled_ADC_y;
	double percent_y;	
	int i = 0;
	
	scaled_ADC_y  = y_ADC_val - scale_factor; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor;
	y_pot_val = (uint16_t)(percent_y * 255); 
		
	// Check if user is not moving
	if (get_direction(get_y_scaled()) == 0){
		digitalPotWrite(STEERING, 125);
		return;
	}
	
	// Determine delay time based on joystick position 
	if (y_pot_val < 131 && y_pot_val > 70){
		delay_time = 7000; 
		steer_dir_left = FULL_LEFT;
		steer_dir_right = FULL_RIGHT; 
	}else if (y_pot_val <= 70){
		delay_time = 8500; 
		steer_dir_left = MID_LEFT;
		steer_dir_right = MID_RIGHT;
	}

	// Go right for the delay time 
	for (i=0; i<delay_time; i++){
		go_right(3300, 1200, scale_factor, division_factor, steer_dir_right);
	}
		
	// Check if user is not moving
	if (get_direction(get_y_scaled()) == 0){
		digitalPotWrite(STEERING, 125);
		return;
	}
		
	// Go left for the delay time 
	for (i=0; i<delay_time; i++){
		go_left(3300, 1200, scale_factor, division_factor, steer_dir_left);
	}

	// Check if user is not moving
	if (get_direction(get_y_scaled()) == 0){
		digitalPotWrite(STEERING, 125);
		return;
	}
}


/*
* Name:					right_circle()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Automatically perform a right circle based on y-axis output 
*/
void right_circle(uint16_t y_ADC_val, uint16_t speed_ADC_val, int scale_factor, int division_factor){
	float scaled_ADC_y;
	double percent_y;		
	uint16_t x_pot_val, y_pot_val;
	uint16_t max_reverse_speed = get_max_reverse_speed(speed_ADC_val);	
	uint16_t max_forward_speed = get_max_forward_speed(speed_ADC_val);	
	
	scaled_ADC_y  = y_ADC_val - scale_factor; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor;
	x_pot_val = (uint16_t)(percent_y * 255); 
	y_pot_val = (uint16_t)(percent_y * 255); 

	// Adjust speed based on limits 
	y_pot_val = adjust_speed(y_pot_val, max_reverse_speed, max_forward_speed); 
		
	if (x_pot_val >= 115){
		x_pot_val = 115; 
	}
	
	digitalPotWrite(THROTTLE, y_pot_val);
	digitalPotWrite(STEERING, x_pot_val);
}


/*
* Name:					left_circle()
* Paramaters: 	uint16_t = y-axis joystick output 
* Description:  Automatically perform a left circle based on y-axis output 
*/
void left_circle(uint16_t y_ADC_val, uint16_t speed_ADC_val, int scale_factor, int division_factor){
	float scaled_ADC_y;
	double percent_y;		
	uint16_t x_pot_val, y_pot_val;
	uint16_t max_reverse_speed = get_max_reverse_speed(speed_ADC_val);	
	uint16_t max_forward_speed = get_max_forward_speed(speed_ADC_val);	
	
	scaled_ADC_y  = y_ADC_val - scale_factor; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor;
	x_pot_val = (uint16_t)(percent_y * 255); 
	y_pot_val = (uint16_t)(percent_y * 255); 

	// Adjust speed based on limits 
	y_pot_val = adjust_speed(y_pot_val, max_reverse_speed, max_forward_speed); 
		
	if (x_pot_val <= 115){
		x_pot_val = 115; 
	}
	
	digitalPotWrite(THROTTLE, y_pot_val);
	digitalPotWrite(STEERING, x_pot_val);
}


//------------ Fully Atonomous Routines ------------//
void self_driving_mode(void){
	if (GPIO_ReadInputDataBit(GPIOC, OBJECT_DETECTION_SW) == 1){
		avoid_object_self_driving(object_detect());
	}else{
		// Self Driving Switch is enabled but Object Detection switch is not
		// Flash the LED quickly to indicate that the OBJ Detection SW needs to be turned on 
		GPIO_SetBits(GPIOB, GPIO_Pin_8);
		delay(500000);
		GPIO_ResetBits(GPIOB, GPIO_Pin_8);
		delay(200000);
		return;
	}
}

