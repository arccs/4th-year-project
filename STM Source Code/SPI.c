/******************************************************************************
 * Name:    SPI.c
 * Description: SPI initialization and functions
 * Version: V1.00
 * Authors: Jon Giambattista
 *
 *----------------------------------------------------------------------------
 * Details: SPI initialization and communications to the digital potentiometers
 *					
 *****************************************************************************/
#include "SPI.h" 
#include "LCD_lib.h"


/*
* Name:					SPI_init()
* Paramaters: 	
* Description: 	initilize SPI2 on pins PB12, PB13, PB14
*/
void SPI_init(void){
	// Turn on all clocks for SPI2, GPIOB, and AFIO 
	RCC->APB1ENR |=  RCC_APB1ENR_SPI2EN; 		// SPI2 Clock
	RCC->APB2ENR |=  RCC_APB2ENR_IOPBEN; 		// GPIO B Clock
	RCC->APB2ENR |=  RCC_APB2ENR_AFIOEN; 		// AFIO Clock
	
	// ------ Configure Pins ------  
	// MOSI(PB15) as an alternate function push-pull output,
	// MISO(PB14) as a floating input, 
	// SCK(PB13) as an alternate function push-pull output, 
	// and NSS(PB12) as a general purpose push-pull output.
	GPIOB->CRH = 0xB4B30000; 
	
	// Set the initial state of NSS(PB12) high by writing to the GPIOB_BSRR.
	GPIOB->BSRR |= NSS_LOW; 
	
	SPI2->CR1 &= 0x3040; // Clear SPI settings
	// SPI settings:
	//   - master mode
	//   - 2 lines full duplex
	//   - CPOL low
	//   - CPHA 1 edge
	//   - data size 8 bit
	//   - MSB first bit
	//   - software NSS selection
	SPI2->CR1 |= SPI_CR1_MSTR | SPI_CR1_SSI | SPI_CR1_SSM; 	// Master mode
	SPI2->I2SCFGR &= ~SPI_I2SCFGR_I2SMOD; 									// Clear I2SMOD bit - SPI mode
	SPI2->CRCPR = 7; 																				// Polynomial for CRC calculation
	SPI2->CR1 |= SPI_CR1_SPE;															  // Enable SPI 
}


/*
* Name:					digitalPotWrite()
* Paramaters: 	uint8_t data (POT value 0-256), uint8_t channel (POT channel 0-6)
* Description: 	Write to the digital pot
*								Let CHANNEL_0 = X Axis Output, CHANNEL_1 = Y Axis Output
*/
void digitalPotWrite(uint8_t channel, uint8_t data){	
	// Drive NSS(PB12) low by writing to the GPIOB_BSRR (NOT CS is low)
	GPIOB->BSRR |= NSS_LOW; 
	
	delay(100);

	// Write the channel value to the pot
	SPI2->DR = channel; 				
	
	// Wait for Tx bit in SPI2_SR to be 1
	while(!(SPI2->SR & SPI_SR_TXE)){	
	}; 
		
	// Write the data value of the potentiometer (0-255)
	SPI2->DR = data; 				 
	
	// Wait until busy flag clear
	while((SPI2->SR & SPI_SR_BSY)){	
	}; 
		
	// Drive NSS(PB12) high by writing to the GPIOB_BSRR
	GPIOB->BSRR |= NSS_HIGH;  

	// Done transfer
}
