 /******************************************************************************
 * Name:    gen_init.c
 * Description: General initialization for STM VL D Board
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains general initializations and delay functions
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "main.h"


/*
* Name:					delay()
* Paramaters: 	
* Description: 	Delay based on loop value, 6000 = 1ms
*/
void delay(uint32_t delay)
{
  int i=0;
	for(i=0; i< delay; ++i)
	{
	}
}


/*
* Name:					IO_init()
* Paramaters: 	
* Description: 	Initialize basic digital IO
*/
void IO_init(void)
{
	RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN ; 
	
	GPIOC->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7 ;
	GPIOC->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF2 & ~GPIO_CRL_CNF3 & ~GPIO_CRL_CNF4 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF6 & ~GPIO_CRL_CNF7 ;

	GPIOB->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE5;
	GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF5;
	
	GPIOC->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE8 ;
	GPIOC->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF8 ;
}
