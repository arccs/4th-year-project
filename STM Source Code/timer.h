/******************************************************************************
 * Name:    timer.h
 * Description: Timer initialization and functions 
 * Authors: Jon Giambattista
 *
 *----------------------------------------------------------------------------
 * Details: Used to time operations in the program
 *					
 *****************************************************************************/
#include <stdint.h>
#define CLOCK_SPEED													 ((int16_t)7.2e9); 		// to convert in milliseconds 

void timer_init(void);
void timer_init_with_interrupts(void);		
int16_t timer_start(void);
int16_t timer_stop(int16_t);
void timer_shutdown(void); 
