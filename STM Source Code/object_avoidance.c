/******************************************************************************
 * Name:    object_avoidance.c
 * Description: Functions relating to object avoidance 
 * Author: Jacob Loos, Jon Giambattista 
 *
 *-----------------------------------------------------------------------------
 * Details: 
 *		Functions relating to object avoidance (USART Reading, car control algorithms) 	
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "object_avoidance.h"

/* Private macros ------------------------------------------------------------*/
#define RXBUFFERSIZE 36			// USART Data Frame 

//-------------------------------- USART DATA PROCESSING  -------------------------------//
/*	
* Name:					object_detect()
* Paramaters: 	void
* Description: 	Returns an int based on where an object is in relation to the car:
								object_position = 0 then no obects 
 								object_position = 1 then object on the left corner 	
 								object_position = 2 then object on the right corner  
 								object_position = 3 then object on the left side 				
 								object_position = 4 then object on the right side 
 								object_position = 5 then object in the middle
*/
int object_detect()
{
	uint16_t recieve_all[RXBUFFERSIZE];			// Data Frame 
	uint16_t valuable_bytes[20];						// Useful data from frame
	int i, hold, left, right, middle;				// Sensor Values
	static int threshold;										// Sensitivity threshold
	int object_position;									
	int timeout_flag; 											// Timout flag for USART
	
	threshold = get_object_threshold();			// Fetch object detection sensitivity threshold from the slider pot
	
	for (i = 0; i < RXBUFFERSIZE; i++) 
	{ 	
		timeout_flag = 0;   							 
			
		do
		{
			timeout_flag++;
			if(timeout_flag == 100000)     			// If timeout is exceeded xBee communications are down 
			{
				return 10;    										// return an exception value 
			}else{
				GPIO_SetBits(GPIOB, GPIO_Pin_8);	// LED on
			}
		}
		// Read the xBee data into the buffer 
		while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET); 
		recieve_all[i] = USART_ReceiveData(USART1);            
	}
		
	hold = 0;
		
	// Loops through the buffer until 0x1e (30) is found, this byte occures 2 spots before the data bytes
	// Data bytes are then recovered and stored in another array, 2 bytes per IR sensor
	// Once data bytes are found and stored the loop is terminated 
	for (i = 0; i < RXBUFFERSIZE; i++)    
	{ 
		if(recieve_all[i] == 0x7e)
		{
			while(hold < 19)          
			{
				valuable_bytes[hold] = recieve_all[i];
				hold++;
				i++;	
			}
			i=RXBUFFERSIZE;
		}
	}
	
	// Converts the bytes into a useful CM value
	left = valuable_bytes[12] + (valuable_bytes[11] * 256);			 // Left
 	right = valuable_bytes[14] + (valuable_bytes[13] * 256);   	 // Right
	middle = valuable_bytes[16] + (valuable_bytes[15] * 256);    // middle
	
	left = 4800/(left-20);	
	right = 4800/(right-20);
	middle = 4800/(middle-20);
	
	
	// Finds the correct value for a car movement function 
	if (middle < threshold && middle > 0){
		object_position = 5; 	
	}else if (left < threshold && left > 0 && middle < threshold && middle > 0){
		object_position = 1;
	}
	else if (right < threshold && right > 0 && middle < threshold && middle > 0){
		object_position = 2;
	}
	else if (left < threshold && left > 0){
		object_position = 3;
	}          
	else if (right < threshold && right > 0){
		object_position = 4;
	}else{
		object_position = 0;
	}
	
	return object_position; 
}


//-------------------------------- OBJECT AVOIDANCE CONTROL -------------------------------//
//------------ Object Detection Functions ------------//
/*
* Name:					avoid_object()
* Paramaters: 	int = obj_position (position of the object relative to the 4 IR sensors)  
* Description:  Re-route the car's movement if objects are detected 
*/
bool avoid_object(int obj_position){	
	static int middle_counter; 
	int j=0; 
	
	if (obj_position == 10){		// USART TIMEOUT
		// STOP THE CAR and blink the LED to indicate loss of xBEE communications
		digitalPotWrite(THROTTLE, STOPPED);	
		digitalPotWrite(STEERING, CENTRE_STEER);
		GPIO_SetBits(GPIOB, GPIO_Pin_8);
		delay(600000);
		GPIO_ResetBits(GPIOB, GPIO_Pin_8);
		delay(600000);
		return true; 
	}
	
	if (obj_position == 5){
		middle_counter++; 
	}else{
		middle_counter = 0; 
	}
		
	switch (obj_position){
		case 0:
			// No object detected
			return false; 
		case 1: 
			// Object on Left Corner
			digitalPotWrite(STEERING, FULL_RIGHT);
			digitalPotWrite(THROTTLE, get_y_pot_val());		// Fetch users current y-axis position 
			return true; 
		case 2: 
			// Object on Right Corner
			digitalPotWrite(STEERING, FULL_LEFT);
			digitalPotWrite(THROTTLE, get_y_pot_val());
			return true; 	
		case 3: 
			// Object on Left
			digitalPotWrite(STEERING, FULL_RIGHT);
			digitalPotWrite(THROTTLE, get_y_pot_val());
			return true; 		
		case 4: 
			// Object on Right 
			digitalPotWrite(STEERING, FULL_LEFT);
			digitalPotWrite(THROTTLE, get_y_pot_val());
			return true; 				
		case 5: 
			if (middle_counter > 1 && middle_counter <= 3){
				// Assist Breaking 
				for (j=0; j<1000; j++){
					digitalPotWrite(THROTTLE, MIN_REV_SPEED); 
				}
				return true; 
			}
			if (get_y_ADC() < 120){
				// Assist Breaking 
				digitalPotWrite(THROTTLE, STOPPED); 
				return true;
			}else{
				return false; 
			}				
		}
	return false; 
}


/*
* Name:					avoid_object_self_driving()
* Paramaters: 	int = obj_position (position of the object relative to the 4 IR sensors)  
* Description:  Determines how to re-route the cars movement for self driving mode	
*/
void avoid_object_self_driving(int obj_position){	
	static int averaging;			// Ensures we get 2 of the same responses from the xBee in a row
	uint16_t self_driving_speed = get_speed_self_driving(get_speed_ADC()); 
	
	if (obj_position == 0){		// If no object 
	 	digitalPotWrite(STEERING, 128);		// Steer staight 
	  averaging = 0; 					// Reset the averaging 
	}else{
		averaging++; 						// Object Detected - increase averaging counter
	}
	
	if (obj_position == 10){		// USART TIMEOUT
		// STOP THE CAR and blink the LED to indicate loss of xBEE communications
		digitalPotWrite(THROTTLE, STOPPED);	
		digitalPotWrite(STEERING, CENTRE_STEER);
		
		GPIO_SetBits(GPIOB, GPIO_Pin_8);
		delay(600000);
		GPIO_ResetBits(GPIOB, GPIO_Pin_8);
		delay(600000);
		return; 
	}
	
	// Steer the Car based on the objects positions 
	if (obj_position == 0 && averaging < 1){ 
		// No object 
		digitalPotWrite(STEERING, 128);
		digitalPotWrite(THROTTLE, self_driving_speed);
	}else if (obj_position == 1 && averaging >= 1){
		// Object on Left - Middle 
		if (averaging > 2 && averaging < 5){
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
		if (averaging > 15){
				assist_REV(SLIGHT_RIGHT);
		}else{
			digitalPotWrite(STEERING, SLIGHT_LEFT);
			digitalPotWrite(THROTTLE, self_driving_speed);
		}
	}else if (obj_position == 2 && averaging >= 1){
		// Object on Right - Middle 
		if (averaging > 2 && averaging < 5){
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
			
		if (averaging > 15){
				assist_REV(SLIGHT_LEFT);
		}else{
			digitalPotWrite(STEERING, SLIGHT_RIGHT);
			digitalPotWrite(THROTTLE, self_driving_speed);
		}
	}else if (obj_position == 3 && averaging >= 1){
		// Object on Right 
		if (averaging > 2 && averaging < 5){
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
			
		if (averaging > 15){
			assist_REV(FULL_LEFT);
		}else{	
			digitalPotWrite(STEERING, FULL_RIGHT);
			digitalPotWrite(THROTTLE, self_driving_speed);
		}
	}else if (obj_position == 4 && averaging >= 1){
		if (averaging > 2 && averaging < 5){
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
		
		if (averaging > 15){
			assist_REV(FULL_RIGHT);
		}else{	
			// Object on Right Hand Side
			digitalPotWrite(STEERING, FULL_LEFT);
			digitalPotWrite(THROTTLE, self_driving_speed);
		}
	}else if (obj_position == 5 && averaging >= 1){
		if (averaging > 2 && averaging < 5){
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
		
		// Middle 
		if (averaging > 15){
			assist_REV(FULL_LEFT); 
		}
		else{	
			// Object on Right Hand Side
			digitalPotWrite(STEERING, FULL_LEFT);
			digitalPotWrite(THROTTLE, MIN_REV_SPEED);
		}
	}
}


/*
* Name:					assist_REV()
* Paramaters: 	steering_dir is the direction of steering (digital pot val 0-255) 
* Description:  Assists the car in going into reverse while in self-driving mode 	
*/
void assist_REV(uint16_t steering_dir){
	int i = 0; 
	for (i=0; i<2000; i++){
		digitalPotWrite(STEERING, steering_dir);
		digitalPotWrite(THROTTLE, 125);	
	}
	for (i=125; i<MIN_REV_SPEED; i++){
		digitalPotWrite(STEERING, steering_dir);
		digitalPotWrite(THROTTLE, i);	
		delay(1000);
	}						
	for (i=0; i<10000;i++){
		digitalPotWrite(THROTTLE, 140);	
	}
}


/*
* Name:					assist_break()
* Paramaters: 	steering_dir is the direction of steering (digital pot val 0-255) 
* Description:  Assists the car in breaking suddenly
*/
void assist_break(uint16_t steering_dir){
	int i = 0; 
	for (i=125; i<MIN_REV_SPEED; i++){
		digitalPotWrite(STEERING, steering_dir);
		digitalPotWrite(THROTTLE, i);	
		delay(1000);
	}						
}


/*
* Name:					get_object_threshold()
* Paramaters: 	nil
* Description:  Fetch obj_detection pot threshold from main and process it
*/
int get_object_threshold(void){
	uint16_t threshold_ADC_val = get_obj_pot_ADC(); 	// Fetch ADC value from the DMA (logic done in main.c)
	uint16_t threshold_distance;  
	
	// Range of sensors is from 40cm to 10cm
	threshold_distance = (float)(threshold_ADC_val * 16/ 4095); 
	
	// Threshold distance is now from 0-50cm
	// Add 10cm for offset
	threshold_distance = threshold_distance + 15;
	
	
	// Return value from 10-80cm
	return (int)threshold_distance; 
}
