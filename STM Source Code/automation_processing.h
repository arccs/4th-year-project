/******************************************************************************
 * Name:    automation_processing.h
 * Description: Functions relating to automating the cars movements 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Figure 8 control, and other semi-automonous modes
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include <cstdio>

/* Private Macros ------------------------------------------------------------*/
#define STOPPED	131
#define MAX_FW_SPEED 15
#define MID_FW_SPEED 60
#define MIN_FW_SPEED 100
#define MAX_REV_SPEED 255
#define MID_REV_SPEED	196
#define MIN_REV_SPEED 138
#define CENTRE_STEER 131
#define SLIGHT_LEFT 80
#define MID_LEFT 50
#define FULL_LEFT 5
#define SLIGHT_RIGHT 145
#define MID_RIGHT 200 
#define FULL_RIGHT 250

/* Function Prototypes --------------------------------------------------------*/
void go_forward(uint16_t); 
void go_backward(uint16_t); 
void go_left(uint16_t, uint16_t, int, int, uint16_t);
void go_right(uint16_t, uint16_t, int, int, uint16_t);
void figure_eight(uint16_t, uint16_t, int, int);
void right_circle(uint16_t, uint16_t, int, int);
void left_circle(uint16_t, uint16_t, int, int);
void self_driving_mode(void);
void boost_reverse(void);
