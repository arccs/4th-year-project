/******************************************************************************
 * Name:    UART.c
 * Description: Functions relating to xBEE Serial communications link
 * Author: Jacob Loos / Jon Giambattista 
 *
 *****************************************************************************/
 
/* Includes ------------------------------------------------------------------*/
#include "UART.h"

/*
* Name:					USART_GPIOA_init()
* Paramaters: 	
* Description: 	Initialize USART GPIO Pins (PA8, PA9) 
*/
void USART_GPIOA_init(void){
	/* Bit configuration structure for GPIOA PIN8 */
	GPIO_InitTypeDef GPIOA_InitStructure = { GPIO_Pin_8, GPIO_Speed_50MHz, 
																				 GPIO_Mode_Out_PP };
																					 
	/* Enable PORT A clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	/* Initialize GPIOA: 50MHz, PIN8, Push-pull Output */
	GPIO_Init(GPIOA, &GPIOA_InitStructure);   
}


/*
* Name:					USART1_init()
* Paramaters: 	
* Description: 	Initialize USART Peripheral: 9600 Baud, 8-bit data, 1 stop bit
*/
void USART1_init(void){
	/* USART configuration structure for USART1 */
  USART_InitTypeDef USART1_InitStructure;
	
  /* Bit configuration structure for GPIOA PIN9 and PIN10 */
  GPIO_InitTypeDef GPIOA_InitStructure;
	
  /* Enalbe clock for USART1, AFIO and GPIOA */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO | 
                           RCC_APB2Periph_GPIOA, ENABLE);
                            
  /* GPIOA PIN9 alternative function Tx */
  GPIOA_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIOA_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOA_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIOA_InitStructure);

	/* GPIOA PIN10 alternative function Rx */
	GPIOA_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIOA_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOA_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIOA_InitStructure);

	/* Enable USART1 */
	USART_Cmd(USART1, ENABLE);  
	
	/* Baud rate 11520, 8-bit data, One stop bit
	 * No parity, Do both Rx and Tx, No HW flow control
	 */
	USART1_InitStructure.USART_BaudRate = 38400;   
	USART1_InitStructure.USART_WordLength = USART_WordLength_8b;  
	USART1_InitStructure.USART_StopBits = USART_StopBits_1;   
	USART1_InitStructure.USART_Parity = USART_Parity_No ;
	USART1_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART1_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

	/* Configure USART1 */
	USART_Init(USART1, &USART1_InitStructure);
	
	/* Enable the USART */
	USART_Cmd(USART1, ENABLE);  			
}
