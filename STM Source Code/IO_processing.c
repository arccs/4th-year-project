/******************************************************************************
 * Name:    IO_processing.c
 * Description: Process External IO and determine what mode to operate in 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains functions relating to digital IO
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "IO_processing.h"


/*
* Name:					get_RC_Mode()
* Paramaters: 	
* Description: 	Checks GPIO ODR Switch for on or off
* 							ON = 4 Wheels, OFF = 2 Track
*/
int get_RC_mode(void){
	// Check track mode switches  
	if (GPIO_ReadInputDataBit(GPIOC, TRACK_MODE_SW) == 0)		
		{
			// Switch is active 
			// 4 Wheel Mode Enabled (regular cars)
			return 1; 
		}
	else if (GPIO_ReadInputDataBit(GPIOC, TRACK_MODE_SW) == 1){
			// Switch is inactive  
			// 2 Track Mode Enabled (Tanks, etc.)
			return 0; 
		}
	
	return 0; 
}


/*
* Name:					get_special_Mode()
* Paramaters: 	
* Description: 	Checks GPIO ODR Switches for figure 8 mode, or self-driving mode (Both only work on 4-wheel vehicles)
* 							
*/
int get_special_mode(void){
	// Check Figure 8 Switch
	if (GPIO_ReadInputDataBit(GPIOC, FIGURE_EIGHT_MODE_SW) == 1)
		{
			// Switch is active 
			// Figure 8 mode enabled
			return 1; 
		}
	else if (GPIO_ReadInputDataBit(GPIOC, SELF_DRIVING_MODE_SW) == 1){
			// Switch is active   
			// Self-Driving Mode Enabled 
			return 2; 
		}
	else if (GPIO_ReadInputDataBit(GPIOC, FIGURE_EIGHT_MODE_SW) == 0 && GPIO_ReadInputDataBit(GPIOC, SELF_DRIVING_MODE_SW) == 0){
			// Special functions disabled
			return 0;
	}
		
	return 0; 
}
