/******************************************************************************
 * Name:    main.c
 * Description: main functions
 * Author: Jon Giambattista 
 *
 * Details: Covers the initialization of system functions and peripherals
 * 					Main control loop runs through joystick control routines and checks IO
 *					External functions in this file relate to fetching ADC values, and DMA initialization
 *				
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f10x.h"
#include "LCD_lib.h"
#include "gen_init.h"
#include "object_avoidance.h" 
#include "joystick_periph_setup.h"
#include "joystick_processing.h"
#include "automation_processing.h" 
#include "stm32f10x_gpio.h"
#include "SPI.h"
#include "IO_processing.h"
#include "UART.h"

/* Private macros ------------------------------------------------------------*/
#define ARRAYSIZE 		5
#define ADC1_DR    		((uint32_t)0x4001244C)

/* Private variables ---------------------------------------------------------*/
volatile uint16_t ADC_values[ARRAYSIZE];				// Using a global as only option 

// Calibration Variables
static int scale_factor_y;
static int division_factor_y; 
static int scale_factor_x;
static int division_factor_x; 

/* Private function prototypes -----------------------------------------------*/
void joystick_DMA_init(void);
void joystick_ADC_init(void);


int main(void)
{		
	// Switch Status Variables 
	int RC_car_type; 
	int special_mode; 
	
  /* System clocks configuration --------------------------------------------*/
  RCC_Configuration();
 
  /* GPIO configuration -----------------------------------------------------*/
  GPIO_Configuration();
	
  /* LCD configuration ------------------------------------------------------*/
	LCDio_init();
	LCD_init();
		
  /* ADC configuration ------------------------------------------------------*/
	joystick_DMA_init();
	joystick_ADC_init();
	
	/* SPI configuration ------------------------------------------------------*/
	SPI_init(); 

	/* USART configuration -----------------------------------------------------*/
	USART_GPIOA_init();
	USART1_init();
	LED_Configuration();	
	
  /* Begin ADC Conversion ----------------------------------------------------*/
	DMA_Cmd(DMA1_Channel1, ENABLE); 				//Enable the DMA1 - Channel1
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	
	/* Startup -----------------------------------------------------------------*/
	// Fetch calibration variables (scale/division factors) 
	scale_factor_y = get_scale_factor(0);
	division_factor_y = get_division_factor(0);
	scale_factor_x = get_scale_factor(1);
	division_factor_x = get_division_factor(1);
	
	// Check Car Type switch (requires restart of system to change modes) 
	RC_car_type = get_RC_mode();	
	
	// Main Control Loop 
  while (1)
  { 			
		special_mode = get_special_mode();	// Fetch status of switches	
		
		/* ----- Main Control Loop ----- */
		if (RC_car_type == 1){							// 4-Wheel Vehicles 
			if (special_mode == 1){ 					// Figure 8 enabled
				figure_eight(ADC_values[1], ADC_values[2], scale_factor_y, division_factor_y);			
			}else if(special_mode == 2){			// Self-Driving enabled 
				self_driving_mode(); 
			}else{														// Standard RC Control 
				standard_RC_control(ADC_values[0],ADC_values[1], ADC_values[2], ADC_values[4],
										scale_factor_y, division_factor_y, scale_factor_x, division_factor_x);
			}
		}else{															// 2-Track Vehicles
			// Not within the scope of this project 
			// Having it enabled risks performance of regular style vehicles if swich is accidentaly turned on 
			intToLCD(400);
		}	
	}
}



/********************************** Global ADC Fetch Functions ***********************************/
/*
* Name:					get_x_ADC()
* Paramaters: 	
* Description: 	Return the X ADC (digital_pot) values
*/
uint16_t get_x_ADC(void){
	double percent_x;									// Percentage of ADC input to max AXC input (scaled) 
	uint16_t x_pot_val; 							// Output value to the potentiometer (1-255) 
	
	// Scale is nessisary to change the range of ADC input from 2735-4000 to 0-1265 
	// Limitation exists due to the joystick sensor not 'starting' out at 0V output (instead range is from 2 - 2.91 V) 
	float scaled_ADC_x  = ADC_values[0] - scale_factor_x; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_x =  scaled_ADC_x/division_factor_x;	
	x_pot_val = (uint16_t)(percent_x * 255); 

	return x_pot_val;
}


/*
* Name:					get_y_ADC()
* Paramaters: 	
* Description: 	Return the Y (digital_pot) ADC values
*/
uint16_t get_y_ADC(void){
	double percent_y;									// Percentage of ADC input to max AXC input (scaled) 
	uint16_t y_pot_val; 							// Output value to the potentiometer (1-255) 

	// Scale is nessisary to change the range of ADC input from 2735-4000 to 0-1265 
	// Limitation exists due to the joystick sensor not 'starting' out at 0V output (instead range is from 2 - 2.91 V) 
	float scaled_ADC_y  = ADC_values[1] - scale_factor_y; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor_y;		
	y_pot_val = (uint16_t)(percent_y * 255); 

	return y_pot_val;
}


/*
* Name:					get_y_pot_val()
* Paramaters: 	
* Description: 	Return the Y pot values with speed scaled 
*/
uint16_t get_y_pot_val(void){
	double percent_y;									// Percentage of ADC input to max AXC input (scaled) 
	uint16_t y_pot_val; 							// Output value to the potentiometer (1-255) 
	uint16_t max_reverse_speed = get_max_reverse_speed(ADC_values[2]);	
	uint16_t max_forward_speed = get_max_forward_speed(ADC_values[2]);	
	
	// Scale is nessisary to change the range of ADC input from 2735-4000 to 0-1265 
	// Limitation exists due to the joystick sensor not 'starting' out at 0V output (instead range is from 2 - 2.91 V) 
	float scaled_ADC_y  = ADC_values[1] - scale_factor_y; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor_y;		// 1300 is a 'floating' value (Will change when testing occurs) 
	y_pot_val = (uint16_t)(percent_y * 255); 
	
	y_pot_val = adjust_speed(y_pot_val, max_reverse_speed, max_forward_speed); 

	return y_pot_val;
}



/*
* Name:					get_y_scaled()
* Paramaters: 	
* Description: 	Return the scaled joystick y-ADC values
*/
uint16_t get_y_scaled(void){
	return  ADC_values[1] - scale_factor_y; 
}


/*
* Name:					get_x_ADC_adjusted()
* Paramaters: 	
* Description: 	Return the X ADC values - adjusted to eliminate jitters
*/
uint16_t get_x_ADC_adjusted(int turn_dir){
	float scaled_ADC_y;
	double percent_y;		
	uint16_t x_pot_val;
	
	scaled_ADC_y  = ADC_values[1] - scale_factor_x; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_y =  scaled_ADC_y/division_factor_x;
	x_pot_val = (uint16_t)(percent_y * 255); 
		
	if (turn_dir == 0){ 		// Left 
		if (x_pot_val >= 115){
			x_pot_val = 115; 
		}
	}else{									// Right
		if (x_pot_val <= 115){
			x_pot_val = 115; 
		}
	}
	
	return(x_pot_val);
}


/*
* Name:					get_obj_pot_ADC()
* Paramaters: 	
* Description: 	Return the Object Detection Pot ADC values
*/
uint16_t get_obj_pot_ADC(void){
	return ADC_values[3];
}


/*
* Name:					get_obj_detection_threshold()
* Paramaters: 	
* Description: 	Return the Object Detection Pot threshold as an integer 
*/
uint16_t get_speed_ADC(void){
	return ADC_values[2]; 
}


/********************************** DMA Initialization ***********************************/
/*
* Name:					joystick_DMA_init()
* Paramaters: 	
* Description: 	Configures the DMA (needs to be in main as it writes to the ADC_Values global :|)
*/
/** @Note: Heres a good overview on the DMA: http://www.embedds.com/using-direct-memory-access-dma-in-stm23-projects/
**/
void joystick_DMA_init(void)
{
	DMA_InitTypeDef DMA_InitStructure;

  /* DMA1 channel1 configuration ----------------------------------------------*/
	DMA_DeInit(DMA1_Channel1);
	
	// channel will be used for memory to memory transfer
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = 5;	// Corresponds to # of ADC channels in use 
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	// source and destination start addresses
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC1_DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)ADC_values;
	
	// send values to DMA registers
	DMA_Init(DMA1_Channel1, &DMA_InitStructure);
	
	// Enable DMA1 Channel Transfer Complete interrupt
	DMA_Cmd(DMA1_Channel1, ENABLE); 								//Enable the DMA1 - Channel1
}
