/******************************************************************************
 * Name:    IO_processing.c
 * Description: Process External IO and determine what mode to operate in 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains functions relating to digital IO
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32f10x.h"

/* Private Macros ------------------------------------------------------------*/
#define TRACK_MODE	GPIOC
#define TRACK_MODE_PIN						 8

#define PROGRESSIVE_ACCEL	GPIOC
#define PROGRESSIVE_ACCEL_PIN	 		 9

#define SELF_DRIVING_MODE	GPIOC
#define SELF_DRIVING_MODE_PIN			 10

#define FIGURE_EIGHT_MODE	GPIOC
#define FIGURE_EIGHT_MODE_PIN			 11

#define OBJECT_DETECTION	GPIOC
#define OBJECT_DETECTION_PIN	 		 12

#define TRACK_MODE_SW               ((uint16_t)0x0100)  
#define PROGRESSIVE_ACCEL_SW        ((uint16_t)0x0200)  
#define SELF_DRIVING_MODE_SW        ((uint16_t)0x0400)  
#define FIGURE_EIGHT_MODE_SW        ((uint16_t)0x0800)  
#define OBJECT_DETECTION_SW      	  ((uint16_t)0x1000)  

/* Function Protoypes --------------------------------------------------------*/
int get_RC_mode(void);
int get_special_mode(void); 
