/******************************************************************************
 * Name:    joystick_periph_setup.c
 * Description: Joystick DMA, ADC and DAC initialization Funcations  
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains peripheral setups and clock initializations. 
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "joystick_periph_setup.h"


/*
* Name:					joystick_ADC_init()
* Paramaters: 	
* Description: 	Initialize ADC1 channels 2 and 3 pins PA2,PA3
*/
void joystick_ADC_init(void)
{
	ADC_InitTypeDef ADC_InitStructure;

  /* ADC1 configuration ------------------------------------------------------*/
  ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_InitStructure.ADC_ScanConvMode = ENABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfChannel = 5;
  ADC_Init(ADC1, &ADC_InitStructure);

  /* ADC1 regular channel14 configuration */ 
  ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_28Cycles5);			// X - Input - PA0
  ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 2, ADC_SampleTime_28Cycles5);			// Y - Input - PA1
  ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 3, ADC_SampleTime_55Cycles5);			// Speed Control - PA2
  ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 4, ADC_SampleTime_55Cycles5);			// Distance Control	- PA3
  ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 5, ADC_SampleTime_55Cycles5);			// Joystick Reference	-PA4

  /* Enable ADC1 DMA */
  ADC_DMACmd(ADC1, ENABLE);
  
  /* Enable ADC1 */
  ADC_Cmd(ADC1, ENABLE);

  /* Enable ADC1 reset calibration register */   
  ADC_ResetCalibration(ADC1);
  /* Check the end of ADC1 reset calibration register */
  while(ADC_GetResetCalibrationStatus(ADC1));

  /* Start ADC1 calibration */
  ADC_StartCalibration(ADC1);
	
  /* Check the end of ADC1 calibration */
  while(ADC_GetCalibrationStatus(ADC1));
}


/*
* Name:					RCC_Configuration()
* Paramaters: 	
* Description: 	Configure clocks relating to DMA, ADC and GPIO
*/
void RCC_Configuration(void)
{
  RCC_ADCCLKConfig(RCC_PCLK2_Div2);
 
  /* Enable DMA1 clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

  /* Enable GPIOs and ADC1 clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC |
                         RCC_APB2Periph_ADC1, ENABLE);
}


/*
* Name:					GPIO_Configuration()
* Paramaters: 	
* Description: 	Configure GPIO For ADC and USART Tx
*/
void GPIO_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

	// USART TX init 	
	GPIOB->CRL |= GPIO_CRL_MODE7;					// Mode Config
	GPIOB->CRL &= ~GPIO_CRL_MODE7;				// Port Config
	RCC->APB2ENR |=  RCC_APB2ENR_IOPAEN ; 

	// Setup PA1, PA2 for Analog Input
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}


/*
* Name:					LED_Configuration()
* Paramaters: 	
* Description: 	Configure GPIO For Object Avoidance LED
*/
void LED_Configuration(void){	
	GPIOB->CRH |= GPIO_CRH_MODE8;				// Mode Config
	GPIOB->CRH &= ~GPIO_CRH_CNF8;		
}


/*------------------ ASSERT CHECK ------------------*/
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif
