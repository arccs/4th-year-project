/******************************************************************************
 * Name:    SPI.c
 * Description: SPI initialization and functions
 * Version: V1.00
 * Authors: Jon Giambattista
 *
 *----------------------------------------------------------------------------
 * Details: SPI initialization and communications to the digital potentiometers
 *					
 *****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "gen_init.h"
#include "stm32f10x.h"

/* Private Macros ------------------------------------------------------------*/
#define NSS_HIGH	    0x00001000 		// PB12
#define NSS_LOW				0x10000000		// PB12
#define MAX_POT_VALUE 254; 
#define MIN_POT_VALUE 0; 
 
/* Function Prototypes --------------------------------------------------------*/
void SPI_init(void); 
void digitalPotWrite(uint8_t, uint8_t);
 
