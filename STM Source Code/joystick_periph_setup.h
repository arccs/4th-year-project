/******************************************************************************
 * Name:    joystick_periph_setup.c
 * Description: Joystick DMA, ADC and DAC initialization Funcations  
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains peripheral setups and clock initializations. 
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include <cstdio>
 
/* Function Prototypes --------------------------------------------------------*/
void joystick_ADC_init(void);
void RCC_Configuration(void);
void GPIO_Configuration(void);
void LED_Configuration(void);
void assert_failed(uint8_t* file, uint32_t );
