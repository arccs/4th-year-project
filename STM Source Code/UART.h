/******************************************************************************
 * Name:    UART.h
 * Description: Functions relating to cBEE Serial communications link
 * Author: Jacob Loos/Jon Giambattista
 *
 *****************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "gen_init.h"

/* Function Prototypes --------------------------------------------------------*/
void USART_GPIOA_init(void); 
void USART1_init(void);
