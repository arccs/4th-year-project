/******************************************************************************
 * Name:    main.h
 * Description: main functions
 * Author: Jon Giambattista 
 *
 * Details: Covers the initialization of system functions and peripherals
 * 					Main control loop runs through joystick control routines and checks IO
 *					External functions in this file relate to fetching ADC values, and DMA initialization
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Private function prototypes -----------------------------------------------*/
void joystick_DMA_init(void);
void DMA1_Channel1_IRQHandler(void);
void USART_DMA_Configuration(void);
uint16_t get_x_ADC(void);
uint16_t get_y_ADC(void);
uint16_t get_y_scaled(void);
uint16_t get_x_ADC_adjusted(int);
uint16_t get_obj_pot_ADC(void);
uint16_t get_y_pot_val(void);
uint16_t get_speed_ADC(void); 

