 /******************************************************************************
 * Name:    gen_init.c
 * Description: General initialization for STM VL D Board
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains general initializations and delay functions
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Function Prototypes --------------------------------------------------------*/
void delay(uint32_t delay);
void IO_init(void);


