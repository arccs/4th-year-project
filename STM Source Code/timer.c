/******************************************************************************
 * Name:    timer.c
 * Description: Timer initialization and functions 
 * Authors: Jon Giambattista
 *
 *----------------------------------------------------------------------------
 * Details: Used to time operations in the program
 *					
 *****************************************************************************/
#include <stdint.h>
#include "timer.h"
#include "stm32f10x.h"


/*
* Name:                 void timer_init()
* Paramaters:   
* Description:  Initialize the STMF103 TIM2 timer. 
* 							configured as a continuous free running countdown timer
*								wraps around to 0xFFFF when 0x0000 is crossed 
*/ 
void timer_init(void){
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; 
	
	TIM2->ARR = 0xFFFF; 										// Auto reload 
	TIM2->CR1 |= 0x0010; 										// Downcounter mode 
	TIM2->EGR |= 0x1;												// Generate update event at underflow
	
	TIM2->PSC = 0; 													// Timer Speed = Clock Speed
	TIM2->CR1 |= TIM_CR1_CEN; 							// Enable
}	


/*
* Name:                 void timer_init_with_interrupts()
* Paramaters:   
* Description:  Initialize the STMF103 TIM3 timer. 
* 							configured to link an LED at 1Hz.
*/ 
void timer_init_with_interrupts(void){
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; 
	
	TIM3->ARR = 10000; 											// Set to 10000 so that we get CLK/ARR = 1Hz
	TIM3->PSC = 7199; 											// 72Mhz/7199+1 = 10000
	TIM3->DIER = TIM_DIER_UIE;
	TIM3->CR1 |= TIM_CR1_CEN; 							// Enable
}


/*
* Name:                 void timer_shutdown()
* Paramaters:   
* Description:  Shutdown the timer 
*/ 
void timer_shutdown(void){
	TIM2->CR1 = 0x0000; 							// Disable Timer
}


/*
* Name:                 int16_t timer_start()
* Paramaters:   
* Description:  Return value of the timer 
*/ 
int16_t timer_start(void){
	return (TIM2->CNT);
}


/*
* Name:                 int16_t timer_stop()
* Paramaters:   
* Description:  Return value of the timer (while detecting wraparounds)
*/ 
int16_t timer_stop(int16_t start_time){
	int16_t currentTime = (TIM2->CNT);
	int16_t elapsedTime = (start_time - currentTime);
	
	// Detect wraparound 
	if (currentTime > start_time){		
			// Add the difference of wrap to elapsed time 
			elapsedTime = start_time + (0xFFFF - currentTime);
	}
	
	return (elapsedTime);
}
