/******************************************************************************
 * Name:    object_avoidance.h
 * Description: Functions relating to object avoidance 
 * Author: Jacob Loos, Jon Giambattista 
 *
 *-----------------------------------------------------------------------------
 * Details: 
 *		Functions relating to object avoidance (USART Reading, car control algorithms) 				
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "UART.h"
#include "gen_init.h"
#include <stdlib.h>     
#include <stdbool.h>
#include "LCD_lib.h"
#include "main.h"
#include "automation_processing.h"
#include "SPI.h"
#include "joystick_processing.h"

/* Function Prototypes --------------------------------------------------------*/
// Object Detection
int object_detect(void);
bool avoid_object(int); 
void avoid_object_self_driving(int);
int get_object_threshold(void); 
void assist_REV(uint16_t); 
void assist_break(uint16_t);

