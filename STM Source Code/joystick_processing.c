/******************************************************************************
 * Name:    joystick_processing.c
 * Description: Joystick Processing Functions 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: Contains the main control code for joystick operation
 * 					main function of concerin is standard_RC_control
 *					
******************************************************************************/

/* Includes -----------------------------------------------------------------*/
#include "joystick_processing.h"
#include "automation_processing.h"
#include "object_avoidance.h"
#include "gen_init.h"
#include "SPI.h"
#include "LCD_lib.h"
#include "joystick_periph_setup.h"
#include <stdlib.h>   
#include <cstdio>
#include <stdbool.h>
#include "main.h"
#include "UART.h"
#include "IO_processing.h"


/* Private Variables --------------------------------------------------------	
 - Lookup Tables for Speed Pot Control
 - To improve performance over manually calculating speed position for both FW and REV
 - Const keyword should ensure storage in FLASH to save SRAM memory 
*/
static const uint16_t reverse_speed_LUT[140] =
{
	131, 131, 131, 131, 131, 133, 134, 134, 135, 135, 136, 136, 137, 137,
	138, 138, 139, 139, 140, 140, 141, 141, 142, 142, 143, 143, 144, 144, 
	145, 145, 146, 146, 147, 147, 148, 148, 149, 149, 150, 150, 151, 151, 
	152, 152, 153, 153, 154, 154, 155, 155, 156, 156, 157, 157, 158, 158, 
	159, 159, 160, 160, 161, 161, 162, 162, 163, 163, 164, 164, 165, 165, 
	166, 166, 167, 167, 168, 168, 169, 169, 170, 170, 171, 171, 172, 172, 
	173, 173, 174, 174, 175, 175, 176, 176, 177, 177, 178, 178, 179, 179, 
	180, 180, 181, 181, 182, 182, 183, 183, 184, 184, 185, 185, 186, 186, 
	187, 187, 188, 188, 189, 189, 190, 190, 191, 191, 192, 192, 193, 193, 
	194, 194, 195, 195, 196, 196, 197, 197, 198, 198, 199, 199
};

static const uint16_t forward_speed_LUT[130] =
{
	131, 131, 131, 131, 124, 123, 122, 121, 120, 119, 118, 117, 116,
	115, 114, 113, 112, 111, 110, 109, 108, 107, 106, 105, 104, 103,
	102, 101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87,
	86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70,
	69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 
	52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36,
	35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19,
	18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 
};
 

//-------------------------------- 4-Wheel Joystick Routines -------------------------------//
/*
* Name:					standard_joystick_Control()
* Paramaters: 	uint16_t x_ADC_Val (0-4095 X-Axis input), uint16_t y_ADC_Val (0-4095 y-Axis input)
*								uint16_t speed_ADC_Val (0-4095 Speed_Pot input)
* Description:  Standard RC Car (4-wheel) control routine
*/
void standard_RC_control(uint16_t x_ADC_val,
												 uint16_t y_ADC_val,
												 uint16_t speed_ADC_val,
												 uint16_t centre_ADC_val,
												 int scale_factor_y, int division_factor_y,
												 int scale_factor_x, int division_factor_x){
	// Recall X = Steering, Y = throttle 
	double percent_x;										// Percentage of ADC input to max AXC input (scaled) 
	double percent_y;			
	uint16_t x_pot_val; 								// Output value to the potentiometer (1-255) 
	uint16_t y_pot_val; 		
	uint16_t max_reverse_speed = get_max_reverse_speed(speed_ADC_val);	
	uint16_t max_forward_speed = get_max_forward_speed(speed_ADC_val);	
	float scaled_ADC_x;
	float scaled_ADC_y;
	static int reverse_adjust_flag; 		// >1 = reverse active,  0 = forward active
	int boost_reverse = 131; 
	bool object_was_detected; 
	bool progressive_accel_enabled; 
											
	// Lower limit on y_axis 
	if (y_ADC_val < 2740){
		y_ADC_val = 2740;
	}
	
	// Scale ADC Values 
	// Scale is nessisary to change the range of ADC input from 2735-4000 to 0-1265 
	// See get_scale_factor function below for more information 
	scaled_ADC_x  = x_ADC_val - scale_factor_x; 
	scaled_ADC_y  = y_ADC_val - scale_factor_y; 
	
	// Determine percentage of digital pot resistance (0-255) 
	percent_x =  scaled_ADC_x/division_factor_x;		
	percent_y =  scaled_ADC_y/division_factor_y;	
	
	// Check for zero points and output to zero (weird things happen here..) 
	if (percent_x == 0){
		digitalPotWrite(STEERING, 1);
	}else if (percent_y == 0){
		digitalPotWrite(THROTTLE, 1);
	}else{
		x_pot_val = (uint16_t)(percent_x * 255); 
		y_pot_val = (uint16_t)(percent_y * 255); 
		
		// Adjust speed based on limits 
		y_pot_val = adjust_speed(y_pot_val, max_reverse_speed, max_forward_speed); 
		
		// Check Object Detection Switch 
		if (GPIO_ReadInputDataBit(GPIOC, OBJECT_DETECTION_SW) == 1)
		{
			object_was_detected = avoid_object(object_detect());
		}else{
			GPIO_ResetBits(GPIOB, GPIO_Pin_8);
			object_was_detected = false; 
		}
		
		// Check Progressive Acceleration Switch 
		if (GPIO_ReadInputDataBit(GPIOC, PROGRESSIVE_ACCEL_SW) == 1)
		{
			// Enable Progressive Accel
			progressive_accel(max_reverse_speed, max_forward_speed, scaled_ADC_y, object_was_detected);
			progressive_accel_enabled = true; 
		}else{
			progressive_accel_enabled = false; 
		}
	
		// Stop noise at resting position (hold digital pot output steady at centre X&Y position)
		if (y_pot_val >= 126 && y_pot_val <= 133){
			y_pot_val = 130; 
		}
		if (x_pot_val >= 119 && x_pot_val <= 126){
			x_pot_val = 123; 
		}
		
		// Check reverse flag -- Time how long the joystick is being held in the reverse position 
		if (y_pot_val > 140){ 		// Reverse Active
			reverse_adjust_flag++;
		}else{
			reverse_adjust_flag=0; 
		}
		
		// Override cars Electronic Speed Control Breaking feature -- go straight to reverse 
		if (progressive_accel_enabled == false && object_was_detected == false)
		reverse_assist(reverse_adjust_flag, y_pot_val, boost_reverse);

		// Output signals to the RC transmitter 
		if (object_was_detected == false){
			digitalPotWrite(STEERING, x_pot_val);
			digitalPotWrite(THROTTLE, y_pot_val);
		}
	}
}


//-------------------------------- 2-Track Joystick Routines -------------------------------//
/*
* Name:					two_track_RC_control()
* Paramaters: 	uint16_t x_ADC_Val (0-4095 X-Axis input), uint16_t y_ADC_Val (0-4095 y-Axis input)
*								uint16_t speed_ADC_Val (0-4095 Speed_Pot input), uint16_t centre_ADC_val (0-4095 VREF input)
* Description:  Track Based RC Car control routine - shares many similatities with 4-wheel routine
*/
void two_track_RC_control(uint16_t x_ADC_val,
												 uint16_t y_ADC_val,
												 uint16_t speed_ADC_val,
												 uint16_t centre_ADC_val,
												 int scale_factor_y, int division_factor_y,
												 int scale_factor_x, int division_factor_x){
	// X = Magnitude of Speed Scalar, Y = Speed/Direction
	double percent_x;																		// Percentage of ADC input to max AXC input (scaled) 
	double percent_y;			
	uint16_t y_pot_val; 		
	uint16_t x_pot_val; 																 
	uint16_t max_reverse_speed = get_max_reverse_speed(speed_ADC_val);	
	uint16_t max_forward_speed = get_max_forward_speed(speed_ADC_val);	
	float scaled_ADC_x;
	float scaled_ADC_y;
	int direction; 
	
	// Scale ADC Values 
	// Scale is nessisary to change the range of ADC input from 2735-4000 to 0-1265 
	// See get_scale_factor function below for more information 
	scaled_ADC_x  = x_ADC_val - scale_factor_x; 
	scaled_ADC_y  = y_ADC_val - scale_factor_y; 
													 
	// Determine percentage of digital pot resistance (0-255) 
	percent_x =  scaled_ADC_x/division_factor_x;		
	percent_y =  scaled_ADC_y/division_factor_y;	
	
	if (percent_x == 0){
		digitalPotWrite(STEERING, 1);
	}else if (percent_y == 0){
		digitalPotWrite(THROTTLE, 1);
	}else{
		y_pot_val = (uint16_t)(percent_y * 255); 
		x_pot_val = (uint16_t)(percent_x * 255); 

		// Adjust speed based on limits 
		y_pot_val = adjust_speed(y_pot_val, max_reverse_speed, max_forward_speed); 
		direction = get_direction(scaled_ADC_y); 
		
		// Perform two track operations 
		perform_two_track_operations(direction, y_pot_val, x_pot_val);
	}
}


/*
* Name:					perform_two_track_operations()
* Paramaters: 	
* Description:  Perform 2 track operations (tank type vehicles) 
*/
void perform_two_track_operations(int direction, uint16_t y_pot_val, uint16_t x_pot_val){
	// Begin by setting both tracks to the value of the y-axis pot 	
	uint16_t l_pot_val = y_pot_val; 		// Left Track
	uint16_t r_pot_val = y_pot_val; 		// Right Track 
	
	// Adjust for steering
	if (direction == 0){
		return; 
	}else{
		if (x_pot_val < 123){		// Left
			l_pot_val = l_pot_val - x_pot_val; 
		}else{									// Right
			r_pot_val = r_pot_val - x_pot_val; 
		}
	}
	
	// Output to the vehicle
	digitalPotWrite(STEERING, r_pot_val);
	digitalPotWrite(THROTTLE, l_pot_val);
}


//-------------------------------- General Helper Routines -------------------------------//
/*
* Name:					progressive_accel(float)
* Paramaters: 	Self Explanitory 
* Description:  Progressive acceleration and Decceleration
*/
void progressive_accel(uint16_t max_reverse_speed, uint16_t max_forward_speed, float scaled_ADC_y, bool object_was_detected){
	int i = 0;
	int j = 0; 
	int direction;
	static bool moving;
	static int reverse_adjust_flag; 					// >1 = reverse active,  0 = forward active
	int boost_reverse = 131; 
	
	direction = get_direction(scaled_ADC_y);  // Fetch direction of the joystick 

	if (direction == 0){			// Stopped 
		moving = true;
	}
	
	if (object_was_detected == false){
		digitalPotWrite(STEERING, get_x_ADC());	 // Output steering value 
	}
	
	// Check reverse flag 
	if (get_y_pot_val() > 140){ 		// Reverse Active
		reverse_adjust_flag++;
	}else{
		reverse_adjust_flag=0; 
	}
	
	reverse_assist(reverse_adjust_flag, get_y_pot_val(), boost_reverse);

	if (moving == true){
		if (direction == 1){				// Forward
			for (i=111; i > max_forward_speed; i--){			// Loop through from stopped until the forward speed limit is met
				if (get_direction(get_y_scaled()) == 0 || get_direction(get_y_scaled()) == 2){
					// Assist Breaking 
					for (j=0; j<6000; j++){
						digitalPotWrite(THROTTLE, MID_REV_SPEED); 
					}
					moving = true; 								// Joystick in resting position, stop this routine
					return;
				}

				delay(400000);	// Create artificial acceleartion via delay 
			
				// Check for objects 
				if (GPIO_ReadInputDataBit(GPIOC, OBJECT_DETECTION_SW) == 1)
				{
					object_was_detected = avoid_object(object_detect());
				}else{
					GPIO_ResetBits(GPIOB, GPIO_Pin_8);
					object_was_detected = false; 
				}
				
				if (object_was_detected == false) {
					// Write the speed and throttle values to the D_POT
					digitalPotWrite(STEERING, get_x_ADC());
					digitalPotWrite(THROTTLE, i); 
				}
				
				moving = false; 
			}
		}else if (direction == 2){	// Reverse
			for (i=135; i < max_reverse_speed; i++){		// Loop through from stopped until the reverse speed limit is met
				if (get_direction(get_y_scaled()) == 0 || get_direction(get_y_scaled()) == 1){	
					// Assist Breaking 
					digitalPotWrite(THROTTLE, MIN_FW_SPEED); 
					moving = true; 							// Joystick in resting position, stop this routine
					return;
				}
					
				delay(400000);	// Create artificial acceleartion via delay 
	
				// Write the speed and throttle values to the D_POT
				digitalPotWrite(STEERING, get_x_ADC());
				digitalPotWrite(THROTTLE, i); 
				
				moving = false; 
			}
		}
	}
}


/*
* Name:					get_speed_pot(float)
* Paramaters: 	uint16_t speed_ADC_Val (0-4095 Speed_Pot input)
* Description:  Get value of speed pot  
*/
uint16_t get_speed_pot(uint16_t speed_ADC_Val){
	uint16_t pot_val;		// Max Speed value 
	pot_val = (uint16_t)((speed_ADC_Val * 3.3/4095)/3.3 * 255); 
	
	if (OBJECT_DETECTION->IDR & (1 << OBJECT_DETECTION_PIN))
	{
		// Limit speed when object avoidance on
		pot_val = (uint16_t)((speed_ADC_Val * 3.3/4095)/3.3 * 15)+45; 
	}	
	
	return pot_val; 
}


/*
* Name:					get_speed_self_driving(float)
* Paramaters: 	uint16_t speed_ADC_Val (0-4095 Speed_Pot input)
* Description:  Get value of speed pot (slower for self driving mode) 
*/
uint16_t get_speed_self_driving(uint16_t speed_ADC_Val){
	uint16_t pot_val;		// Max Speed value 
	pot_val = (uint16_t)((speed_ADC_Val * 3.3/4095)/3.3 * 10); 
	
	return (10-pot_val)+100; 
}


/*
* Name:					get_max_reverse_speed(float)
* Paramaters: 	uint16_t speed_ADC_Val (0-4095 Speed_Pot input)
* Description:  Get the max reverse speed (Fetched from Look Up Table) 
*/
uint16_t get_max_reverse_speed(uint16_t speed_ADC_Val){
	uint16_t max_reverse_speed;									// Max Speed value 
	max_reverse_speed = (get_speed_pot(speed_ADC_Val))/2; 
	
	return reverse_speed_LUT[max_reverse_speed]; 		// Fetch speed from LUT
}


/*
* Name:					get_max_forward_speed(float)
* Paramaters: 	uint16_t speed_ADC_Val (0-4095 Speed_Pot input)
* Description:  Get the max forward speed  (Fetched from Look Up Table) 
*/
uint16_t get_max_forward_speed(uint16_t speed_ADC_Val){
	uint16_t max_forward_speed;									// Max Speed value 
	max_forward_speed = (get_speed_pot(speed_ADC_Val))/2; 
	
	return forward_speed_LUT[max_forward_speed]; 		// Fetch speed from LUT
}


/*
* Name:					assist_steering()
* Paramaters: 	float
* Description:  Adjust steering so that max speed and max steering can be achieved at the same time 
*/
uint16_t assist_steering(uint16_t x_pot_val){
	if (x_pot_val > 135){
		x_pot_val = 245;
	}
	else if (x_pot_val < 105){
		x_pot_val = 8; 
	}
	
  return x_pot_val; 
}


/*
* Name:					get_direction()
* Paramaters: 	float
* Description:  Return the car's intended direction as a bool 
*/
int get_direction(uint16_t scaled_ADC_y){	
	if (scaled_ADC_y <= 720 && scaled_ADC_y >= 530){
		return 0; 	// Not moving
	}else if (scaled_ADC_y < 530){
		return 1;  	// Forwards
	}else{
		return 2;		// Backwards 
	}
}

//-------------------------------- Other Functions -------------------------------//
/*
* Name:					adjust_speed()
* Paramaters: 	
* Description:  Adjust speed based on limits
*/
uint16_t adjust_speed(uint16_t y_pot_val, uint16_t max_reverse_speed, uint16_t max_forward_speed){
	// Adjust speed based on limits 
	if (y_pot_val > max_reverse_speed){
		y_pot_val = max_reverse_speed; 
	}else if (y_pot_val < max_forward_speed){
	  y_pot_val = max_forward_speed; 
	}
	return y_pot_val;
}


/*
* Name:					reverse_assist()
* Paramaters: 	
* Description:  Override RC Car's breaking feature and go straight to reverse from forwards 
*/
void reverse_assist(int reverse_adjust_flag, uint16_t y_pot_val, int boost_reverse){
	if (reverse_adjust_flag > 8 && reverse_adjust_flag <= 9){
		// Force forward output to the car to go from 'brake' to 'reverse' 
		for (boost_reverse = 125; boost_reverse < 135; boost_reverse++){ 				
			commandToLCD(LCD_LN2);
			intToLCD(boost_reverse); 
			
			if(boost_reverse > y_pot_val){
				return; 
			}
			digitalPotWrite(THROTTLE, boost_reverse);
		}
	}
}


/*
* Name:					zero_pots()
* Paramaters: 	
* Description:  Zero potentiometer values (CH 1->6) to half resistance 
*/
void zero_pots(void){
	int i = 0;
	
	// Zero unused potentiometer values to minimum resistance 
	for (i = 2; i < 6; i ++){
		digitalPotWrite(i, 1);
	}
	
	// Set x and y pots to half the resistance available 
	digitalPotWrite(0, 131); 
	digitalPotWrite(1, 131); 
}


//-------------------------------- Calibration Functions -------------------------------//
/* 
	In order for the joystick to operate properly it is necessary to determine the centre, max,
	and min points of the X and Y positions. Since the joystick outputs a voltage to the STM ADC from
	~2V to ~3V the range of ADC inputs is not linear from 0-4096 (more like 2500-4000). In order for the
	joystick processing code to work calibration is nessisary to scale the ADC values to 0-Z 
	(z being the value at the scaled 3v input). 

	With a scale from 0-Z it is easy to set a digital pot output from 0-255. Calibration is nessisary for
	running off any voltage aside from the specced 12v. 
	
	==== Steps to Calibrate ==== 
	1. Read the voltage on the Y output of the LM358N Op Amp 
	2. Print the Y ADC values to the LCD (main while loop)
	3. Record the min, max and centre point voltages and ADC values
	4. Let scale_factor = the minimum adc value 
	5. To obtain the division_factor subract the max ADC value from the scale_factor
*/

/*
* Name:					get_scale_factor()
* Paramaters: 	
* Description:  Get the scale factor based on input voltage
*/
uint16_t get_scale_factor(int axis){
	if (axis == 0){	
		return 2680;		// Y - axis 
	}else{				
		return 2650; 		// X - axis
	}
}


/*
* Name:					get_division_factor()
* Paramaters: 	
* Description:  Get the division factor based on input voltage
*/
uint16_t get_division_factor(int axis){
	if (axis == 0){	
		return 1350;		// Y - axis 
	}else{				
		return 1275; 		// X - axis
	}
}
