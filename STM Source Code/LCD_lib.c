/******************************************************************************
 * Name:    LCD_lib.c
 * Description: Hitachi LCD initialization and functions 
 * Author: Jon Giambattista 
 *
 *----------------------------------------------------------------------------
 * Details: LCD initialization functions 
 *					
*****************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "LCD_lib.h"
#include "gen_init.h"
#include "stm32f10x.h"
#include <stdio.h>
#include <string.h>

/*
* Name:					LCDio_init()
* Paramaters: 	
* Description: Configure pins for LCD Initialization 
*/
void LCDio_init (void){
	// Enable peripheral clocks for various ports and subsystems
  // Bit 4: Port C Bit3: Port B Bit 2: Port A 
	RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN ; 

	//Set the config and mode bits for PB0, PB5, PB1, PC0-PC7
	// be push-pull outputs (up to 50 MHz)
	GPIOB->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE2 | GPIO_CRL_MODE1;				// Mode Config
	GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF1;					// Port Config\
	
	GPIOC->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7;
	GPIOC->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF2 & ~GPIO_CRL_CNF3 & ~GPIO_CRL_CNF4 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF6 & ~GPIO_CRL_CNF7;
}


/*
* Name:					commandToLCD()
* Paramaters:  uint8_t data - command to transmit to the LCD
* Description: Send a command to the LCD (example - LCD_LN1 = refresh cursor to line 1) 
*/
void commandToLCD(uint8_t data)
{
	GPIOB->BSRR = LCD_CM_ENA; //RS low, E high
	
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	delay(8000);
	
	GPIOB->BSRR = LCD_CM_DIS; //RS low, E low
	delay(80000);
}


/*
* Name:				 dataToLCD()
* Paramaters:  uint8_t data - data to transmit to the LCD
* Description: Send data to the LCD 
*/
void dataToLCD(uint8_t data)
{
	GPIOB->BSRR = LCD_DM_ENA; //RS low, E high
	
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	delay(8000);
	
	GPIOB->BSRR = LCD_DM_DIS; //RS low, E low
	delay(80000);
}


/*
* Name:				 LCD_init()
* Paramaters:  
* Description: Initialize the LCD
*/
void LCD_init(void){
	delay(8000); 	// Wait at least 4.1ms or in this case 5ms

	// Send: 0011xxxx or 0x38 to the command buffer
	commandToLCD(LCD_8B2L); 
	delay(8000); 	// Wait at least 4.1ms or in this case 5ms
	
	// Send: 0011xxxx or 0x38 to the command buffer
	commandToLCD(LCD_8B2L); 
	delay(8000); 	// Wait at least 100us or in this case 1ms
	
	// Send: 0011xxxx or 0x38 to the command buffer
	commandToLCD(LCD_8B2L); 
	delay(8000); 	// Wait at least 1ms or in this case 1ms
	
	// Send again 
	commandToLCD(LCD_8B2L); 
	delay(8000); 	// Wait at least 1ms or in this case 1ms

	// Set the display config
	commandToLCD(LCD_DCB);   // Send 00001111 to enable display, cursor, blink
	delay(8000); 	// Wait at least 1ms or in this case 1ms
	
	commandToLCD(LCD_CLR); 	 // Send 00000001 to go home and clear LCD
	delay(8000); 	// Wait at least 1ms or in this case 1ms

	commandToLCD(LCD_MCR);   // Send 00000110 to move cursor right 
	delay(8000); 	// Wait at least 1ms or in this case 1ms

	// Done
}

/*
* Name:				 stringToLCD()
* Paramaters:  char* - String to send to the LCD
* Description: Sends a char array to the LCD Screen 
*/
void stringToLCD(char *message){
	 int i = 0;
	 uint16_t messagelength = strlen(message); 
	 
	 // Transmit each char one at a time 
	 for (i=0; i < messagelength; ++i)
	 {
		 dataToLCD(*message);
		 ++message; 
	 }
 }

 
/*
* Name:				 intToLCD()
* Paramaters:  int_data - Integer data to transmit
* Description: Sends an integer to the LCD Screen 
*/
 void intToLCD(uint16_t int_data){
	 char port_str[8] = {0};
	 
	 sprintf(port_str, "%d", (uint16_t)int_data); 
	 stringToLCD(port_str); 
}
