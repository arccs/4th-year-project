Design Consistency Status Report
--------------------------------

Report File:        Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1 (PCB - Design Consistency Status Report).txt
Report Written:     Tuesday, February 09, 2016
Design Path:        Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1.pcb
Design Title:       
Created:            2/2/2016 7:59:51 PM
Last Saved:         2/9/2016 2:09:54 PM
Editing Time:       5654 min
Units:              mil (precision 0)


Checking integrity of Schematic "Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1.sch" with PCB "Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1.pcb"
==================================================================================================================================================================================

Ignoring instance TP1 ("TESTLOOP2") as it is defined for PCB only (no Schematic information).
Ignoring instance TP2 ("TESTLOOP2") as it is defined for PCB only (no Schematic information).
Ignoring instance TP3 ("TESTLOOP2") as it is defined for PCB only (no Schematic information).
Ignoring instance TP4 ("TESTLOOP2") as it is defined for PCB only (no Schematic information).
Ignoring instance TP5 ("TESTLOOP2") as it is defined for PCB only (no Schematic information).


End Of Report.
