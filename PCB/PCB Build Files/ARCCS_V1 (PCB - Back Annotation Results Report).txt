Back Annotation Results Report
------------------------------

Report File:        Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1 (PCB - Back Annotation Results Report).txt
Report Written:     Monday, February 08, 2016
Design Path:        Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1.pcb
Design Title:       
Created:            2/2/2016 7:59:51 PM
Last Saved:         2/8/2016 8:08:51 PM
Editing Time:       5112 min
Units:              mil (precision 0)

Back Annotate renames from PCB "Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1.pcb" to Schematic "Z:\Drive\4th Year Project\Electrical\PCB\Working Copy\ARCCS_V1.sch".

Renamed Component from "JP3" to "GND".
Renamed Component from "JP5" to "JP3".
Renamed Component from "PU3" to "U3".


End Of Report.
